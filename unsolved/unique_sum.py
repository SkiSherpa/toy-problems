# Given a list of integers, calculate the sum of the unique entries in the list. This is the same as removing the duplicates from the list, then calculating the sum.

# nums	            Output
# [1, 1, 1]	        1
# [1, 2, 3]	        6
# [1, 3, 1]	        4
# [2, 3, 4, 2, 3]	9

def sum_unique_values(nums):
    pass

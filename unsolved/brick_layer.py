# You have decided to enter the Professional Brick Layer's competition. But, to do that, you have to pass the Basic Brick Laying Coordination Test.

# You are given a certain number of long bricks 5 units long, and a certain number of short bricks 1 unit long. You are asked if you can make a row of bricks goal units long.

# For example, say you are given 3 short bricks and 1 long brick and were asked to make a row 7 units long. You could do that using 1 long brick and two short bricks.

# Say you are given 3 short bricks and 1 long brick and asked to make a row 8 units long. You could do that by using all of the bricks.

# Say you are given 1 short brick and 2 long bricks and asked to make a row 8 units long. You could not do that because you can't break bricks into smaller pieces.

# num_short	    num_long	goal	Output
# 3	            1	        7	    True
# 3	            1	        8	    True
# 3	            1	        9	    False
# 1	            2	        8	    False
# Complete the function can_make_row to return True if you can make a row goal units long with the provided bricks, or to return False if you cannot.
def can_make_row(num_short, num_long, goal):
    pass

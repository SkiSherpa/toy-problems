/*
Given an integer array nums sorted in non-decreasing order, remove the duplicates in-place such that each unique element appears only once. The relative order of the elements should be kept the same. Then return the number of unique elements in nums.

Consider the number of unique elements of nums to be k, to get accepted, you need to do the following things:

Change the array nums such that the first k elements of nums contain the unique elements in the order they were present in nums initially. The remaining elements of nums are not important as well as the size of nums.
Return k.
Custom Judge:

The judge will test your solution with the following code:

int[] nums = [...]; // Input array
int[] expectedNums = [...]; // The expected answer with correct length

int k = removeDuplicates(nums); // Calls your implementation

assert k == expectedNums.length;
for (int i = 0; i < k; i++) {
    assert nums[i] == expectedNums[i];
}
If all assertions pass, then your solution will be accepted.



Example 1:

Input: nums = [1,1,2]
Output: 2, nums = [1,2,_] -> [1,2,1]
Explanation: Your function should return k = 2, with the first two elements of nums being 1 and 2 respectively.
It does not matter what you leave beyond the returned k (hence they are underscores).
Example 2:

Input: nums = [0,0,1,1,1,2,2,3,3,4]
Output: 5, nums = [0,1,2,3,4,_,_,_,_,_] -> [0,1,2,3,4,0,1,1,2,3]
Explanation: Your function should return k = 5, with the first five elements of nums being 0, 1, 2, 3, and 4 respectively.
It does not matter what you leave beyond the returned k (hence they are underscores).


Constraints:

1 <= nums.length <= 3 * 104
-100 <= nums[i] <= 100
nums is sorted in non-decreasing order.
*/

/**
 * @param {number[]} nums
 * @return {number}
 */

// two pointer, i and j, where j >= i+1
// initialize a count = 0
// while (i < length of nums)
// if nums[i] === nums[j]
    // bubble up until new number
    // count++
    // continue
// otherwise, nums[i] !== nums[j]
    // i++
    // j++
// return count

// [i0,j0,1,1,1,2,2,3,3,4] -> [0,i0,j1,1,1,2,2,3,3,4] -> [0,1,i0,j1,1...]

var _removeDuplicates = function(nums) {
    let i = 0;
    let j = 0;
    let count = 0;
    let len = nums.length;
    while (i < len) { // n
        // console.log("i", i, "nums[i]", nums[i]);
        // console.log("j", j, "nums[j]", nums[j]);
        if (nums[i] === nums[j]) {
            count++;
            nums[j] = Infinity;
            j++
        } else { // nums[i] !== nums[j]
            i=j;
            j++;
        }

    }
    nums.sort(); // nlog(n)
    console.log(nums);
    return count;
};
// going for passing all tests

var removeDuplicates = function(nums) {
    let len = nums.length;
    // some edge cases
    if (len === 0) {
        return 0
    }
    if (len === 1) {
        return 1;
    }
    if (len === 2) {
        if (nums[0] === nums[1]) {
            nums[1] = Infinity;
            return 1;
        } else { // nums[0] !== nums[1]
            return 2;
        }
    }
    // i needs to start at 0 in case nums is all a single value
    let i = 0;
    let j = 1;
    let count = 0;
    while (i < len) {
        // console.log("i", i, "nums[i]", nums[i]);
        // console.log("j", j, "nums[j]", nums[j]);
        if (nums[i] === nums[j]) {
            nums[j] = Infinity;
            j++
        } else { // nums[i] !== nums[j]
            count++;
            i=j;
            j++;
        }
    }
    // basic sort can not do negative and positive numbers together
    // have to provide additional function to handle + & - nums together
    nums.sort(function(a,b){
        return a - b;
    });
    return count;
};
// t = 104ms, 20.%  | m = 50.46Mb, 5.06%

// t = n + nlog(n) ~ O(nlog(n))
// m = O(1)

// console.log(removeDuplicates([0,0,1,1,1,2,2,3,3,4]));
// console.log(removeDuplicates([1,1,2]));
// console.log(removeDuplicates([1,2]));
console.log(removeDuplicates([-3,-1,0,0,0,3,3]));

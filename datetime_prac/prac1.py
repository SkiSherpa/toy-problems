import datetime

today = datetime.date.today()
print(f"today's date is {today}")

time_right_now = datetime.datetime.now()
print(f"the time right now is {time_right_now}")
